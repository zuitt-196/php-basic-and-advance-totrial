<!-- BASIC SYNTAX -->

<!-- this block scope is definde the with the php wit -->

<?php
// echo "hello word i bercome a web delopeer soon";

// shortcut in echo



// DECLARATION OF VARIABLE
$var1 = " HElloe";
$var_num = 19;
// $varenumexample = hjeelo;    // is undefined since constant functiio find as the constant of it 
echo $var1;
echo "<br/>";
echo $var_num;



// DATA TYPES
$text = "hello World";  // string
$number = 19; // number
$number2 = 32;
$float = 2.5; // float
$boolean = false; // bollean
$array = array("Vhong", $float, $number, $boolean); // array 

// This var_dump is the to deifine the what is data type want to show
var_dump($number);
echo "<br/>";
var_dump($text);
echo "<br/>";
var_dump($float);
echo "<br/>";
var_dump($boolean);
echo "<br/>";
var_dump($array);
echo "<br/>";



// Concatation . dot use
echo $text . " " . $number;
echo "<br/>";

// to count the lenght of strin strlen method is the build  php
echo strlen($text);
echo "<br/>";
// to replace the string data whatever to  replace
echo str_replace(" ", "", $text);
echo "<br/>";

// to count the lenght of position of data string 
echo strpos($text, "World");
echo "<br/>";

// Define a named of constabt
// breack the code ,this method define() is the  class of constant that never used if they are same label for definel;
// Define method is build in php the first parameter is define label of define , 2nd parameter is the value of the define method the third is define about if bollean data type 
// which is the contant name can be CAPETALIZE OR LOWECASE or general sensitive 

//CONSTANT
define("Master", "Vhongmaster");
echo Master;
echo "<br/>";


// OPERATOR
echo $number + $number2;
echo "<br/>";
echo $number - $number2;
echo "<br/>";
echo $number * $number2;
echo "<br/>";
echo $number2 % $number;
echo "<br/>";

echo $number ** $number2;
echo "<br/>";

//  initialization with the operator 
echo $number = $number2;
echo $number += $number2;
echo "<br/>";
// IF statemnen
// COMPARISON 

$x = 13;
$y = 13;
// IDENTICAL STATEMNT WITH VALUE ONLY
if ($number == $number2) {
    # code...
    echo "True";
} else {
    echo "False";
}
echo "<br/>";

// IDENTICAL STATEMNT WITH VALUE ONLY
if ($x === $y) {
    # code...
    echo "True";
} else {
    echo "False";
}

echo "<br/>";

// NOT IDENTICAL STATEMNT WITH DATA TYPE OF THE  VALUE
if ($x != $y) {
    # code...
    echo "True";
} else {
    echo "False";
}


echo "<br/>";

// NOT IDENTICAL STATEMNT WITH DATA TYPE OF THE  VALUE
if ($x !== $y) {
    # code...
    echo "True";
} else {
    echo "False";
}
echo "<br/>";

if ($x > $y) {
    # code...
    echo "True";
} else {
    echo "False";
}

echo "<br/>";


// lOGICAL OPERATOR 
$num = 1;
$num2 = 4;
$mango = "yellow";
$size = "small";
// OR and || are same
// ADD and && are same
// orx are satify if bewween of it is meet  or both of them make one only 
if ($mango == "yellow" xor $size == "big") {
    echo "True";
} elseif ($mango == "green" && $size == "small") {
    echo "ok true";
} else {
    echo "False";
}
// SWITCH STATEMENTS 
echo "<br/>";


switch ($mango) {
    case 'green':
        echo " Hilaw na manga";
        break;

    case 'yellow':
        echo " Hinog na manga";
        break;

    default:
        echo "lata na mangga";
        break;
}

// LOOP
echo "<br/>";
$a = 1;

$data_type = array(1, 4, 3, 5, 6, 7);
// WHILE LOOP
// while ($i <= 10) {
//     # code...

//     echo " Number " . $i . " <br>";
//     if ($i === 8) {
//         # code...
//         echo "Kani na number" . $i . "<br/>";
//     }
//     $i++;
// }

// DO WHILE LOOOP

// do {
//     echo "number" . $a . "<br/>";
//     $a++;
// } while ($a <= 10);

//FOR LOOP

// for ($i = 0; $i <= 10; $i++) {
//     echo "Kani na number " . $i . " <br/>";
// }

//FOR EACH LOOP
foreach ($data_type as $values) {
    echo $values . "<br/>";
}

?>