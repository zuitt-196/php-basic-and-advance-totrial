<?php
class Person
{
    var $name = 'John';

    // method
    function set_name($new_name)
    {
        $this->name = $new_name;
    }

    function get_name()
    {
        return $this->name;
    }
}


class Boy extends Person
{
    var $height;

    // method

    function set_height($new_height)
    {
        $this->height = $new_height;
    }

    function get_height()
    {
        return $this->height;
    }
}
